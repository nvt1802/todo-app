const paths = [
  {
    title: 'Todo',
    path: '/todo',
  },
  {
    title: 'create | Todo',
    path: '/todo/create',
  },
  {
    title: 'update | Todo',
    path: '/todo/update',
  },
];
export default defineNuxtRouteMiddleware((context) => {
  const { path } = context;
  const res = paths.find((item: any) => item.path === path);
  // console.log(res);

  if (res) {
    useHead({ title: res.title });
  }
});
