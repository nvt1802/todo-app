export default defineEventHandler(async (event: any) => {
  const config = useRuntimeConfig();
  const path: string = config.public.apiUrl;
  const { method, headers: _ } = event.req;

  switch (method) {
    case 'GET':
      return await $fetch(`${path}/${event.context.params.id}`, { method: 'GET' });
    case 'PUT':
      const body = await useBody(event);
      return await $fetch(`${path}/${event.context.params.id}`, { method: 'PUT', body });
    case 'DELETE':
      return await $fetch(`${path}/${event.context.params.id}`, { method: 'DELETE' });
    default:
      return {};
  }
});
