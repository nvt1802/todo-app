export default defineEventHandler(async (event: any) => {
  const config = useRuntimeConfig();
  const path: string = config.public.apiUrl;
  const { method, headers: _ } = event.req;

  switch (method) {
    case 'GET':
      return await $fetch(path);
    case 'POST':
      const body = await useBody(event);
      return await $fetch(path, { method: 'POST', body });
    default:
      return {};
  }
});
