export const useCount = () => {
  const nuxtApp = useNuxtApp();
  return useState('count', () => 0);
};
