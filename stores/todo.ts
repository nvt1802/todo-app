import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useTodoStore = defineStore('todo', () => {
  const todoList = ref([]);
  const data = ref({});

  async function addNewTaskAction(newValue: any) {
    try {
      const res = await $fetch(`/api/todo`, {
        method: 'POST',
        body: newValue,
      });
      return res;
    } catch (error) {
      console.error(error);
    }
  }

  async function updateTaskAction(id: string, newValue: any) {
    try {
      await $fetch(`/api/todo/${id}`, {
        method: 'PUT',
        body: newValue,
      });
      await fetchListTaksAction();
    } catch (error) {
      console.error(error);
    }
  }

  async function removeTaskByIdAction(taskId: string) {
    const res: any = await $fetch(`/api/todo/${taskId}`, {
      method: 'DELETE',
    });
    todoList.value = todoList.value.filter((item: any) => !taskId.includes(item.id));
  }

  async function fetchListTaksAction() {
    const res: any = await $fetch('/api/todo', {
      method: 'GET',
    });
    todoList.value = res;
  }

  async function getTaskById(taskId: string) {
    console.log(taskId);

    const res: any = await $fetch(`/api/todo/${taskId}`, {
      method: 'GET',
    });
    return res;
  }

  return { todoList, getTaskById, addNewTaskAction, removeTaskByIdAction, fetchListTaksAction, updateTaskAction };
});
